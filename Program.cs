﻿using System;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
namespace Student_s_Servant_Bot
{
    class Program
    {
        private static TelegramBotClient client;

        private static string token = "1650799522:AAFwOuhCnMqyXZlxHMcANGSvBJhmP-MWZ5M";
        private static ReplyKeyboardMarkup keyboardWhatINeed;
        private static ReplyKeyboardMarkup keyboardGameTech;
        static void Main(string[] args)
        {

            client = new TelegramBotClient(token); //{Timeout = TimeSpan.FromSeconds(10)};
            client.OnMessage += Bot_OnMessage;

            client.StartReceiving();
            InitKeyboardGameTech();
            InitKeyboardWhatINeed();

            Console.ReadKey();
        }

        private static async void Bot_OnMessage(object sender, MessageEventArgs e)
        {
            string message = e?.Message?.Text;
            switch (message.ToLower())
            {

                case Command.start:
                    string start =
 @"Командуй мной: 
/start - запусти меня
/showMeWhatYouCan - покажу, что умею";
                    await client.SendTextMessageAsync(e.Message.Chat.Id, start, replyMarkup: keyboardWhatINeed);
                    break;
                case Command.gameTech:
                    await client.SendTextMessageAsync(e.Message.Chat.Id, "Маринка лучшая <3", replyMarkup: keyboardGameTech);
                    break;
                case "2":
                    await client.SendTextMessageAsync(e.Message.Chat.Id, "Маринка самая красивая <3");
                    break;
                case "3":
                    await client.SendTextMessageAsync(e.Message.Chat.Id, "Маринка самая Лучшая <3");
                    break;
                case "4":
                    await client.SendTextMessageAsync(e.Message.Chat.Id, "Маринка самая Любимая <3");
                    break;

                default:
                    await client.SendTextMessageAsync(e.Message.Chat.Id, "Я не понимаю, что вы хотите", replyMarkup: keyboardWhatINeed);
                    break;
            }
        }

        private static void InitKeyboardWhatINeed()
        {
            keyboardWhatINeed = new ReplyKeyboardMarkup(new KeyboardButton[][] {
                new KeyboardButton[] {new KeyboardButton("Игротехника"), new KeyboardButton("2") },
                new KeyboardButton[] {new KeyboardButton("3"), new KeyboardButton("4")}


            }, resizeKeyboard: true);


        }


        private static void InitKeyboardGameTech()
        {
            keyboardGameTech = keyboardWhatINeed = new ReplyKeyboardMarkup(new KeyboardButton[][] {
                new KeyboardButton[] {new KeyboardButton("Список игр"), new KeyboardButton("Компоненты игротехники") },
                new KeyboardButton[] {new KeyboardButton("Что-то"), new KeyboardButton("То-то") }


            }, resizeKeyboard: true); //oneTimeKeyboard: true

        }
    }
}
